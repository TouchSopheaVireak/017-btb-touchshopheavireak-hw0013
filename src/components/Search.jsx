import React, { Component } from 'react'
import axios from 'axios';
import Display from './Display';

export default class Search extends Component {
    constructor(props) {
        super(props);
        let param = props.location.params.data;
        if (param) {
            console.log(param.value);
            this.state = {
                id: param.value,
                info: {}
            };
        }
    }
    async componentWillMount(){
        await axios.get('http://110.74.194.125:3535/api/articles/'+this.state.id)
            .then(res => {
                this.setState({ info: res.data.data });
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    render() {
        if (this.state.info){
            return <Display>{this.state.info}</Display>
        } else {
            return <h3>null</h3>;
        }
    }
}
