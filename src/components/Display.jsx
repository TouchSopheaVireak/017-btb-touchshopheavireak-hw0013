import React from "react";

const Display = props => {
    return (
        <div>
          <h3>ID :{props.children._id}</h3>
          <h3>Name : {props.children.title}</h3>
          <h3>DES: {props.children.description}</h3>
        </div>
    );
};
export default Display;
