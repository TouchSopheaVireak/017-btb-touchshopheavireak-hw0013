import React, { Component } from 'react';
import {Form,Button, Container} from 'react-bootstrap';
import axios from 'axios';

export default class Edit extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        let param = props.location.params;
        if (param) {
            this.state = {
                id: param.data._id,
                title: param.data.title,
                image: null,
                description: param.data.description,
            };
        }
    }
    handleChange = event => {
        this.setState({
            [event.target.name] : event.target.value
        });
    }
    onFileChange = event => {
        this.setState({ image: event.target.files[0] });
    }
    handleSubmit = async event => {
        event.preventDefault();
        let image_url = '';
            if (this.state.image) {
                const form = new FormData();
                form.append('image', this.state.image);
                console.log(form);
                let url = 'http://110.74.194.125:3535/api/images';
                await axios({
                    url,
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                    data: form,
                  })
                    .then(res => {
                        image_url = res.data.url;
                        console.log(res.data.url)
                    })
                    .catch(err => {
                      console.error(err);
                    });
            }
        const article = {
            title: this.state.title,
            description: this.state.description,
            image: image_url
        }
        await axios.patch(`http://110.74.194.125:3535/api/articles/`+this.state.id,article)
            .then(res => {
                console.log(res);
                this.props.history.push('/home')
            }).catch(function (error) {
                console.log(error);
            });
    }

    fileData = () => { 
        if (this.state.image) { 
            console.log(this.state.image);
          return ( 
            <div>
                <img src={this.state.image}/>
              <h2>File Details:</h2> 
              <p>File Name: {this.state.image.name}</p> 
              <p>File Type: {this.state.image.type}</p> 
              <p> 
                Last Modified:{" "} 
                {this.state.image.lastModifiedDate.toDateString()} 
              </p> 
            </div> 
          ); 
        }
      };
    render() {
        return (
            <Container><br/>
                <h3>Add Article</h3>
                <Form onSubmit={this.handleSubmit}>
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" value={this.state.title} name="title" placeholder="Enter Title" onChange={this.handleChange}/>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" value={this.state.description} name="description" placeholder="Enter Title" onChange={this.handleChange}/>
                        <Form.Label>Files</Form.Label>
                        <input type="file"  name="img" onChange={this.onFileChange} />
                        {this.fileData()} 
                    <Button variant="primary" type="submit">Submit</Button>
                </Form>
            </Container>
        )
    }
}
