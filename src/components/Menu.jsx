import React, { Component } from 'react'
import {Nav,Navbar,FormControl,Form,Button,Table, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    handleChange(event) {
    this.setState({value: event.target.value});
    }
    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
      }
    
    render() {
        return (
            <div>
                 <Navbar bg="dark" variant="dark">
                    <Container>  
                        <Navbar.Brand>AMS</Navbar.Brand>  
                        <Nav className="mr-auto">
                            <Nav.Link as = {Link} to ="/home">Home</Nav.Link>
                        </Nav>
                        <Form inline onSubmit={this.handleSubmit}>
                            <input type="text" value={this.state.value} onChange={this.handleChange} />
                            <Button variant="outline-info" type="submit">
                                <Link to={{pathname: '/Search',params:{data: this.state}}}>Search</Link>
                            </Button>
                        </Form>     
                    </Container>
                </Navbar>
            </div>
        )
    }
}
